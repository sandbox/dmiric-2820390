<?php

namespace Drupal\geonames;

/**
 * Class GeoNamesFactory
 * @package Drupal\geonames
 */
class GeoNamesFactory {

  static function create( $config ) {

    // get geonames configuration
    $config = $config->get('geonames.config');

    $username = $config->get('username');
    $server = $config->get('server');

    $token = $config->get('token');
    $token = !empty($token) ? $token : null;

    return new GeoNames( $username, $server, $token );
  }

}